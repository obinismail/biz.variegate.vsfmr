# README #

Detailed Profit and Loss and Balance Sheet reprot for Openbravo ERP 3.x. An upgrade to the one contained in repository. (I'm just not good with mercurial).

### What is this module ###

* Report only module
* Tabular Detailed Profit and Loss and Balance Sheet report.

### Current known

### Installation ###

* Make sure you have Openbravo ERP installed. Refer to [Installation - Openbravo](http://wiki.openbravo.com/wiki/Installation)
* Git clone into your ERP module folder.
* Run 

```
#!bash

ant update.database compile.src smartbuild
```

### For futher information, you can contact me at ###

* onn.khairuddin@gmail.com