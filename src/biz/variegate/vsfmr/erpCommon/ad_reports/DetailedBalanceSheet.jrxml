<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="DetailedBalanceSheet"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="1190"
		 pageHeight="842"
		 columnWidth="1176"
		 columnSpacing="0"
		 leftMargin="14"
		 rightMargin="0"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<style 
		name="header"
		isDefault="false"
	>

		<conditionalStyle>
			<conditionExpression><![CDATA[new Boolean( $V{level}.compareTo("E") == 0 )]]></conditionExpression>
			<style 
				isDefault="false"
				forecolor="#FFFFFF"
				backcolor="#FCFCFF"
			>
			</style>
		</conditionalStyle>
	</style>

	<parameter name="PERIOD_START" isForPrompting="true" class="java.lang.String">
		<defaultValueExpression ><![CDATA["FD03DF2AB9454E32A0D47C82F7B5CFB2"]]></defaultValueExpression>
	</parameter>
	<parameter name="PERIOD_END" isForPrompting="true" class="java.lang.String">
		<defaultValueExpression ><![CDATA["EE599650D65D49F88BEAD2B6D3E4751D"]]></defaultValueExpression>
	</parameter>
	<parameter name="BASE_WEB" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA["./"]]></defaultValueExpression>
	</parameter>
	<parameter name="AD_CLIENT_ID" isForPrompting="true" class="java.lang.String">
		<defaultValueExpression ><![CDATA["4028817A2E3303E8012E4638003E0268"]]></defaultValueExpression>
	</parameter>
	<parameter name="AD_ORG_ID" isForPrompting="true" class="java.lang.String">
		<defaultValueExpression ><![CDATA["0"]]></defaultValueExpression>
	</parameter>
	<parameter name="C_ACCTSCHEMA_ID" isForPrompting="true" class="java.lang.String">
		<defaultValueExpression ><![CDATA["4028817A2E3303E8012E463807240359"]]></defaultValueExpression>
	</parameter>
	<queryString><![CDATA[SELECT
     (CASE WHEN $P{AD_ORG_ID} = '0' THEN 'Consolidated' ELSE(select name from ad_org where ad_org_id=$P{AD_ORG_ID}) END) as orgname,
     (select name from c_acctschema where c_acctschema_id = $P{C_ACCTSCHEMA_ID}) as schema,
     (select iso_code from c_currency where c_currency_id =(select c_currency_id from c_acctschema where c_acctschema_id = $P{C_ACCTSCHEMA_ID})) AS CURRENCY,
     p.enddate as bs_date,
     e.elementlevel,
     e.ad_client_id,
     $P{AD_ORG_ID} AS ad_org_id,
     (select year from c_year where c_year_id = p.c_year_id) as year,
     p.periodno,
     p.name as period,
     (select se.value from c_elementvalue se join ad_treenode st on se.c_elementvalue_id=st.parent_id
	where e.c_elementvalue_id=st.node_id and ad_tree_id=
	   (select ad_tree_id from ad_tree where treetype = 'EV' and ad_tree.ad_client_id=e.ad_client_id) ) as parentval,
     (select se.name from c_elementvalue se join ad_treenode st on se.c_elementvalue_id=st.parent_id
	where e.c_elementvalue_id=st.node_id and ad_tree_id=
	   (select ad_tree_id from ad_tree where treetype = 'EV' and ad_tree.ad_client_id=e.ad_client_id) ) as parentname,
     e.c_elementvalue_id as account_id,
     (case when e.accounttype = 'O' then 'L' else e.accounttype end) as accounttype,
     null as dateacct,
     e.value as acctvalue,
     e.name as acctdescription,
     (select sum(coalesce(amount,0)) from VSFMR_SUMMARY_V f where dateacct<= p.enddate and (CASE WHEN $P{AD_ORG_ID} = '0' THEN 1=1 ELSE f.ad_org_id=$P{AD_ORG_ID} END) and account_id=e.c_elementvalue_id and f.c_acctschema_id=$P{C_ACCTSCHEMA_ID}) as amount
FROM
     c_elementvalue e, c_period p
WHERE vsfmr_getperiodpseudo(c_period_id) >= vsfmr_getperiodpseudo($P{PERIOD_START}) and vsfmr_getperiodpseudo(c_period_id) <= vsfmr_getperiodpseudo($P{PERIOD_END}) and 
	e.accounttype in ('L','A','O') and e.elementlevel='S'
	and e.ad_client_id = $P{AD_CLIENT_ID}
	and p.ad_client_id = $P{AD_CLIENT_ID}
group by e.elementlevel,
     e.c_elementvalue_id,e.accounttype,e.value,e.name,p.name,
     e.ad_client_id, acctvalue,year,p.periodno,p.enddate

union

SELECT
     (CASE WHEN $P{AD_ORG_ID} = '0' THEN 'Consolidated' ELSE(select name from ad_org where ad_org_id=$P{AD_ORG_ID}) END) as orgname,
     (select name from c_acctschema where c_acctschema_id = $P{C_ACCTSCHEMA_ID}) as schema,
     (select iso_code from c_currency where c_currency_id =(select c_currency_id from c_acctschema where c_acctschema_id = $P{C_ACCTSCHEMA_ID})) AS CURRENCY,
     p.enddate as bs_date,
     e.elementlevel,
     e.ad_client_id,
     $P{AD_ORG_ID} AS ad_org_id,
     (select year from c_year where c_year_id = p.c_year_id) as year,
     p.periodno,
     p.name as period,
     '3900-000' as parentval,
     'Retained Earnings' as parentname,
     '99999999' as account_id,
     'L' as accounttype,
     p.enddate as dateacct,
     '999-100' as acctvalue,
     'Accumulated Profit/(Loss)' as acctdescription,
     sum((select sum(coalesce(amount,0)) from VSFMR_SUMMARY_V f where dateacct<= p.enddate and (CASE WHEN $P{AD_ORG_ID} = '0' THEN 1=1 ELSE f.ad_org_id=$P{AD_ORG_ID} END) and account_id=e.c_elementvalue_id and f.c_acctschema_id = $P{C_ACCTSCHEMA_ID}) ) as amount
FROM
     c_elementvalue e, c_period p
WHERE vsfmr_getperiodpseudo(c_period_id) >= vsfmr_getperiodpseudo($P{PERIOD_START}) and vsfmr_getperiodpseudo(c_period_id) <= vsfmr_getperiodpseudo($P{PERIOD_END}) 
	and e.accounttype in ('E','R') and elementlevel='S'
	and p.ad_client_id = $P{AD_CLIENT_ID}
	and e.ad_client_id = $P{AD_CLIENT_ID}
group by e.elementlevel,
     e.accounttype,p.name,
     e.ad_client_id, acctvalue,year,p.periodno,p.enddate]]></queryString>

	<field name="orgname" class="java.lang.String"/>
	<field name="schema" class="java.lang.String"/>
	<field name="currency" class="java.lang.String"/>
	<field name="bs_date" class="java.sql.Timestamp"/>
	<field name="elementlevel" class="java.lang.String"/>
	<field name="ad_client_id" class="java.lang.String"/>
	<field name="ad_org_id" class="java.lang.String"/>
	<field name="year" class="java.lang.String"/>
	<field name="periodno" class="java.math.BigDecimal"/>
	<field name="period" class="java.lang.String"/>
	<field name="parentval" class="java.lang.String"/>
	<field name="parentname" class="java.lang.String"/>
	<field name="account_id" class="java.lang.String"/>
	<field name="accounttype" class="java.lang.String"/>
	<field name="dateacct" class="java.sql.Timestamp"/>
	<field name="acctvalue" class="java.lang.String"/>
	<field name="acctdescription" class="java.lang.String"/>
	<field name="amount" class="java.math.BigDecimal"/>

		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
				<line direction="TopDown">
					<reportElement
						x="0"
						y="-137"
						width="781"
						height="0"
						forecolor="#808080"
						key="line"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="2.0" lineStyle="Solid"/>
</graphicElement>
				</line>
			</band>
		</title>
		<pageHeader>
			<band height="133"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="35"
						width="431"
						height="21"
						forecolor="#999999"
						key="staticText-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="SansSerif" pdfFontName="Helvetica-BoldOblique" size="14" isBold="true" isItalic="true"/>
					</textElement>
				<text><![CDATA[Detailed Balance Sheet Statement by Period]]></text>
				</staticText>
				<line direction="BottomUp">
					<reportElement
						x="3"
						y="131"
						width="1162"
						height="0"
						forecolor="#000000"
						key="line"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="0.25" lineStyle="Solid"/>
</graphicElement>
				</line>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="1"
						width="507"
						height="28"
						key="textField-1"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="20" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{orgname}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="57"
						width="561"
						height="20"
						key="textField-2"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Oblique" size="12" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["As at " + new SimpleDateFormat("dd-MMM-yyyy").format($F{bs_date})]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="77"
						width="430"
						height="18"
						key="textField-3"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Oblique" size="12" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Schema : " + $F{schema}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="95"
						width="430"
						height="18"
						key="textField-4"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Oblique" size="10" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["* All currency is in " + $F{currency}]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="30"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="572"
						y="6"
						width="170"
						height="19"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font size="10" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="746"
						y="6"
						width="36"
						height="19"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font size="10" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="3"
						width="1162"
						height="0"
						forecolor="#000000"
						key="line"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="2.0" lineStyle="Solid"/>
</graphicElement>
				</line>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="6"
						width="209"
						height="19"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font size="10" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="200"  isSplitAllowed="true" >
				<crosstab >
					<reportElement
						x="0"
						y="16"
						width="1162"
						height="103"
						key="crosstab-1"/>
					<crosstabHeaderCell>						<cellContents mode="Transparent">
					<box></box>
						</cellContents>
					</crosstabHeaderCell>					<rowGroup name="accttype" width="10">
						<bucket>
							<bucketExpression class="java.lang.String"><![CDATA[$F{accounttype}]]></bucketExpression>
						</bucket>
						<crosstabRowHeader>
						<cellContents mode="Transparent">
					<box></box>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="4"
						height="7"
						key="textField-1"/>
					<box></box>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$V{accttype}]]></textFieldExpression>
				</textField>
						</cellContents>
						</crosstabRowHeader>
					</rowGroup>
					<rowGroup name="acctvalue" width="157" totalPosition="End">
						<bucket>
							<bucketExpression class="java.lang.String"><![CDATA[$F{acctvalue} + " " + $F{acctdescription}]]></bucketExpression>
						</bucket>
						<crosstabRowHeader>
						<cellContents mode="Transparent">
					<box></box>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="7"
						y="0"
						width="150"
						height="16"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$V{acctvalue}]]></textFieldExpression>
				</textField>
						</cellContents>
						</crosstabRowHeader>
						<crosstabTotalRowHeader>
						<cellContents mode="Transparent">
					<box>					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.0" lineStyle="Double"/>
</box>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="157"
						height="26"
						key="textField-1"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$V{accttype}.compareTo("A") == 0 ? "Total Assets" : "Total Equity and Liabilities"]]></textFieldExpression>
				</textField>
						</cellContents>
						</crosstabTotalRowHeader>
					</rowGroup>
					<columnGroup name="Period" height="25" headerPosition="Center">
						<bucket order="Descending">
							<bucketExpression class="java.lang.String"><![CDATA[$F{year} + "-" + new DecimalFormat("00").format($F{periodno}) + "\r\n" + $F{period}]]></bucketExpression>
						</bucket>
						<crosstabColumnHeader>
						<cellContents mode="Transparent">
					<box>					<bottomPen lineWidth="1.0" lineColor="#000000"/>
</box>
				<textField isStretchWithOverflow="true" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="7"
						y="0"
						width="64"
						height="25"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$V{Period}]]></textFieldExpression>
				</textField>
						</cellContents>
						</crosstabColumnHeader>
					</columnGroup>

					<measure name="amount_Sum" class="java.math.BigDecimal" calculation="Sum">
						<measureExpression><![CDATA[$F{accounttype}.compareTo("A") == 0 ? $F{amount} : $F{amount}.negate()]]></measureExpression>
					</measure>
					<crosstabCell width="72" height="16">
						<cellContents mode="Transparent">
					<box></box>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;(#,##0.00)" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="7"
						y="0"
						width="64"
						height="16"
						key="textField-1"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{amount_Sum}]]></textFieldExpression>
				</textField>
						</cellContents>
					</crosstabCell>
					<crosstabCell width="0" height="16" columnTotalGroup="Period">
						<cellContents mode="Transparent">
					<box></box>
						</cellContents>
					</crosstabCell>
					<crosstabCell width="72" height="26" rowTotalGroup="acctvalue">
						<cellContents mode="Transparent">
					<box>					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
</box>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;(#,##0.00)" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="7"
						y="0"
						width="64"
						height="26"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[($V{amount_Sum}.intValue()  == 0 ? new BigDecimal(0) : $V{amount_Sum})]]></textFieldExpression>
				</textField>
						</cellContents>
					</crosstabCell>
					<crosstabCell width="0" height="26" rowTotalGroup="acctvalue" columnTotalGroup="Period">
						<cellContents mode="Transparent">
					<box>					<topPen lineWidth="0.5"/>
</box>
						</cellContents>
					</crosstabCell>
					<crosstabCell width="72" height="0" rowTotalGroup="accttype">
						<cellContents mode="Transparent">
					<box></box>
						</cellContents>
					</crosstabCell>
					<crosstabCell width="0" height="0" rowTotalGroup="accttype" columnTotalGroup="Period">
						<cellContents mode="Transparent">
					<box></box>
						</cellContents>
					</crosstabCell>
					<whenNoDataCell>						<cellContents mode="Transparent">
					<box></box>
						</cellContents>
					</whenNoDataCell>					</crosstab>
			</band>
		</summary>
</jasperReport>
