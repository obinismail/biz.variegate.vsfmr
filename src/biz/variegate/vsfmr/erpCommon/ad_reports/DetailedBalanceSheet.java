/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2010 Openbravo SLU 
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package biz.variegate.vsfmr.erpCommon.ad_reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.ad_combos.OrganizationComboData;
import org.openbravo.erpCommon.businessUtility.AccountingSchemaMiscData;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.businessUtility.TreeData;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.info.SelectorUtilityData;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class DetailedBalanceSheet extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strcAcctSchemaId = vars.getGlobalVariable("inpcAcctSchemaId",
          "DetailedBalanceSheet|cAcctSchemaId", "");
      String strPeriodFrom = vars.getGlobalVariable("inpPeriodFromId", "DetailedBalanceSheet|PeriodFrom", "");
      String strPeriodTo = vars.getGlobalVariable("inpPeriodToId", "DetailedBalanceSheet|PeriodTo", "");
      String strOrg = vars.getGlobalVariable("inpOrg", "DetailedBalanceSheet|Org", "");

      printPageDataSheet(response, vars, strPeriodFrom, strPeriodTo, strOrg, strcAcctSchemaId);

    } else if (vars.commandIn("FIND")) {
      String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId",
          "DetailedBalanceSheet|cAcctSchemaId");
      String strPeriodFrom = vars.getRequestGlobalVariable("inpPeriodFromId",
          "DetailedBalanceSheet|PeriodFrom");
      String strPeriodTo = vars.getRequestGlobalVariable("inpPeriodToId", "DetailedBalanceSheet|PeriodTo");
      String strOrg = vars.getRequestGlobalVariable("inpOrg", "DetailedBalanceSheet|Org");

      printPageDataSheet(response, vars, strPeriodFrom, strPeriodTo, strOrg, strcAcctSchemaId);

    } else if (vars.commandIn("PDF", "XLS")) {
      String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId",
          "DetailedBalanceSheet|cAcctSchemaId");
      String strPeriodFrom = vars.getRequestGlobalVariable("inpPeriodFromId",
          "DetailedBalanceSheet|PeriodFrom");
      String strPeriodTo = vars.getRequestGlobalVariable("inpPeriodToId", "DetailedBalanceSheet|PeriodTo");
      String strOrg = vars.getRequestGlobalVariable("inpOrg", "DetailedBalanceSheet|Org");

      if (vars.commandIn("PDF"))
        printPageDataPDF(request, response, vars, strPeriodFrom, strPeriodTo, strOrg, strcAcctSchemaId);
      else
        printPageDataXLS(request, response, vars, strPeriodFrom, strPeriodTo, strOrg, strcAcctSchemaId);

    } else if (vars.commandIn("EDIT_HTML")) {
      String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId",
          "DetailedBalanceSheet|cAcctSchemaId");
      String strPeriodFrom = vars.getRequestGlobalVariable("inpPeriodFromId",
          "DetailedBalanceSheet|PeriodFrom");
      String strPeriodTo = vars.getRequestGlobalVariable("inpPeriodToId", "DetailedBalanceSheet|PeriodTo");
      String strOrg = vars.getRequestGlobalVariable("inpOrg", "DetailedBalanceSheet|Org");

      printPageDataHTML(request, response, vars, strPeriodFrom, strPeriodTo, strOrg, strcAcctSchemaId);

    } else {
      pageError(response);
    }
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strPeriodFrom, String strPeriodTo, String strOrg, String strcAcctSchemaId) throws IOException,
      ServletException {

    String strMessage = "";
    XmlDocument xmlDocument = null;
    // DetailedBalanceSheetData[] data = null;
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();

    String strTreeOrg = TreeData.getTreeOrg(this, vars.getClient());
    String strOrgFamily = getFamily(strTreeOrg, strOrg);

    log4j.debug("Output: DataSheet");

    if (vars.commandIn("FIND")) {
      strMessage = Utility.messageBD(this, "BothDatesCannotBeBlank", vars.getLanguage());
      log4j.warn("Both Period are blank");
    }

    xmlDocument = xmlEngine.readXmlTemplate(
        "biz/variegate/vsfmr/erpCommon/ad_reports/DetailedBalanceSheet").createXmlDocument();

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "DetailedBalanceSheet", false, "", "",
        "imprimir();return false;", false, "ad_reports", strReplaceWith, false, true);
    toolbar.setEmail(false);
    toolbar.prepareSimpleToolBarTemplate();
    toolbar
        .prepareRelationBarTemplate(
            false,
            false,
            "validate(); submitCommandForm('XLS', false, frmMain, 'DetailedBalanceSheetExcel.xls', 'EXCEL');return false;");
    xmlDocument.setParameter("toolbar", toolbar.toString());

    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "biz.variegate.vsfmr.erpCommon.ad_reports.DetailedBalanceSheet");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "DetailedBalanceSheet.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "DetailedBalanceSheet.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    OBError myMessage = vars.getMessage("DetailedBalanceSheet");
    vars.removeMessage("DetailedBalanceSheet");
    if (myMessage != null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }

    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_ORG_ID", "",
          "", Utility.getContext(this, vars, "#AccessibleOrgTree", "DetailedBalanceSheet"),
          Utility.getContext(this, vars, "#User_Client", "DetailedBalanceSheet"), '*');
      comboTableData.fillParameters(null, "DetailedBalanceSheet", "");
      xmlDocument.setData("reportAD_ORGID", "liststructure", comboTableData.select(false));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLE", "C_Period_ID", "C_Period (all)",
          "", Utility.getContext(this, vars, "#AccessibleOrgTree", "DetailedBalanceSheet"),
          Utility.getContext(this, vars, "#User_Client", "DetailedBalanceSheet"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "DetailedBalanceSheet", "");
      xmlDocument.setData("reportPeriodFrom", "liststructure", comboTableData.select(false));
      xmlDocument.setData("reportPeriodTo", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    xmlDocument.setData("reportC_ACCTSCHEMA_ID", "liststructure", AccountingSchemaMiscData
        .selectC_ACCTSCHEMA_ID(this, Utility.getContext(this, vars, "#AccessibleOrgTree",
            "DetailedBalanceSheet"), Utility.getContext(this, vars, "#User_Client",
            "DetailedBalanceSheet"), strcAcctSchemaId));
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("periodFrom", strPeriodFrom);
    xmlDocument.setParameter("periodTo", strPeriodTo);
    xmlDocument.setParameter("adOrgId", strOrg);
    xmlDocument.setParameter("cAcctschemaId", strcAcctSchemaId);
    xmlDocument.setParameter("paramMessage", (strMessage.equals("") ? "" : "alert('" + strMessage
        + "');"));

      if (vars.commandIn("FIND")) {
        // No data has been found. Show warning message.
        xmlDocument.setParameter("messageType", "WARNING");
        xmlDocument.setParameter("messageTitle", Utility.messageBD(this, "ProcessStatus-W", vars
            .getLanguage()));
        xmlDocument.setParameter("messageMessage", Utility.messageBD(this, "NoDataFound", vars
            .getLanguage()));
      }

    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageDataXLS(HttpServletRequest request, HttpServletResponse response,
      VariablesSecureApp vars, String strPeriodFrom, String strPeriodTo, String strOrg,
      String strcAcctSchemaId) throws IOException, ServletException {

    response.setContentType("text/html; charset=UTF-8");
    boolean showDimensions = false;
    String strTreeOrg = TreeData.getTreeOrg(this, vars.getClient());
    String strOrgFamily = getFamily(strTreeOrg, strOrg);

    log4j.debug("Output: XLS report");
    log4j.debug("strTreeOrg: " + strTreeOrg + "strOrgFamily: " + strOrgFamily);

    if (!strPeriodFrom.equals("") && !strPeriodTo.equals("") && !strOrg.equals("")
        && !strcAcctSchemaId.equals("")) {

        String strReportName = "@basedesign@/biz/variegate/vsfmr/erpCommon/ad_reports/DetailedBalanceSheet.jrxml";

        HashMap<String, Object> parameters = new HashMap<String, Object>();

        parameters.put("PERIOD_START", strPeriodFrom);
        parameters.put("PERIOD_END", strPeriodTo);
        parameters.put("AD_CLIENT_ID", vars.getClient());
        parameters.put("AD_ORG_ID", strOrg);
        parameters.put("C_ACCTSCHEMA_ID", strcAcctSchemaId);

        renderJR(vars, response, strReportName, "xls", parameters, null, null);
    } 

  }

  private void printPageDataPDF(HttpServletRequest request, HttpServletResponse response,
      VariablesSecureApp vars, String strPeriodFrom, String strPeriodTo, String strOrg,
      String strcAcctSchemaId) throws IOException, ServletException {

    response.setContentType("text/html; charset=UTF-8");
    String strTreeOrg = TreeData.getTreeOrg(this, vars.getClient());
    String strOrgFamily = getFamily(strTreeOrg, strOrg);

    log4j.debug("Output: PDF report");
    log4j.debug("strPeriodFrom :" + strPeriodFrom + " strPeriodTo :" + strPeriodTo);
    log4j.debug("strClient :" + vars.getClient() + " strOrg :" + strOrg);

    if (!strPeriodFrom.equals("") && !strPeriodTo.equals("") && !strOrg.equals("")
        && !strcAcctSchemaId.equals("")) {
        String strReportName = "@basedesign@/biz/variegate/vsfmr/erpCommon/ad_reports/DetailedBalanceSheet.jrxml";

        HashMap<String, Object> parameters = new HashMap<String, Object>();

        parameters.put("PERIOD_START", strPeriodFrom);
        parameters.put("PERIOD_END", strPeriodTo);
        parameters.put("AD_CLIENT_ID", vars.getClient());
        parameters.put("AD_ORG_ID", strOrg);
        parameters.put("C_ACCTSCHEMA_ID", strcAcctSchemaId);

        renderJR(vars, response, strReportName, "pdf", parameters, null, null);


    } else {
      advisePopUp(request, response, "WARNING", Utility.messageBD(this, "ProcessStatus-W", vars
          .getLanguage()), Utility.messageBD(this, "NoDataFound", vars.getLanguage()));
    }

  }

  private void printPageDataHTML(HttpServletRequest request, HttpServletResponse response,
      VariablesSecureApp vars, String strPeriodFrom, String strPeriodTo, String strOrg,
      String strcAcctSchemaId) throws IOException, ServletException {

    response.setContentType("text/html; charset=UTF-8");
    String strTreeOrg = TreeData.getTreeOrg(this, vars.getClient());
    String strOrgFamily = getFamily(strTreeOrg, strOrg);

    log4j.debug("Output: HTML report");
    log4j.debug("strPeriodFrom :" + strPeriodFrom + " strPeriodTo :" + strPeriodTo);
    log4j.debug("strClient :" + vars.getClient() + " strOrg :" + strOrg);

    if (!strPeriodFrom.equals("") && !strPeriodTo.equals("") && !strOrg.equals("")
        && !strcAcctSchemaId.equals("")) {
        String strReportName = "@basedesign@/biz/variegate/vsfmr/erpCommon/ad_reports/DetailedBalanceSheet.jrxml";

        HashMap<String, Object> parameters = new HashMap<String, Object>();

        parameters.put("PERIOD_START", strPeriodFrom);
        parameters.put("PERIOD_END", strPeriodTo);
        parameters.put("AD_CLIENT_ID", vars.getClient());
        parameters.put("AD_ORG_ID", strOrg);
        parameters.put("C_ACCTSCHEMA_ID", strcAcctSchemaId);

        renderJR(vars, response, strReportName, "html", parameters, null, null);


    } else {
      advisePopUp(request, response, "WARNING", Utility.messageBD(this, "ProcessStatus-W", vars
          .getLanguage()), Utility.messageBD(this, "NoDataFound", vars.getLanguage()));
    }

  }


  private String getFamily(String strTree, String strChild) throws IOException, ServletException {
    return Tree.getMembers(this, strTree, strChild);
  }

  public String getServletInfo() {
    return "Servlet DetailedBalanceSheet. This Servlet was made by Onn Khairuddin based on Trial Balance created by Eduardo Argal and mirurita";
  }
}
